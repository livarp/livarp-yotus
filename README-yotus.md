
==== livarp-yotus | a livarp-based distro with compiz & openbox ====

livarp is a 100% free Debian Wheezy based distro.
his main goal is to make you (re)discover alternative window managers

http://livarp.org

== apps ==

 - shell: bash
 - terminal: terminator, guake, screen, ssh
 - edit & office: vim, evince
 - file managers: ranger, midnightcommander
 - menus & panels: tint2, dmenu, compiz-deskmenu
 - infos & monitoring: htop, ncdu, conky, xosview
 - web browser: iceweasel
 - mail client: mutt
 - irc & tchat: weechat
 - torrent: transmission-cli
 - graphics: gthumb, feh, nitrogen, scrot
 - tools: dzen2, xcolorsel, x11-apps, xdotool, xfonts-terminus, ccze, xosview, pyrenamer
 - misc: most, colortail, curl, bc, libnotify-bin
 - user interface: lxappearance, dmz-cursor-theme, murrine-themes, xcompmgr, arandr
 - multimedia: mocp, mplayer, xfburn
 - archives: file-roller(gui), unrar-free, dtrx(cli) & extract bash-funtion
 - power: upower, acpi, laptop-detect xfce-power-manager
 - filesystems: gparted, dosfstools, ntfs-3g, mtools, ntfsprogs
 - firewall: iptables user script
 - network: wpasupplicant, wireless-tools, network-manager-gnome, network-manager-openvpn-gnome, network-manager-vpnc-gnome, pppoeconf
 - auto-mounting by udisks-glue and dzen2


== installation ==

livarp is available on iso-hybrid format. it can be used :

 - on cd: just burn it
 - on usbdisk with dd command:
        'sudo dd if=livarp-yotus.iso of=/dev/sdX bs=4M;sync'

== documentation ==

- livarp included a complete on-line wiki


#! /bin/bash
# livarp compiz start-up script
###############################

## change caps_lock into super key - for old laptop --------------------
#xmodmap ~/.Xmodmap

## setup auto-mounting -------------------------------------------------
sleep 10 && udisks-glue --session &

## set compiz wallpaper ------------------------------------------------
#nitrogen --restore ## uncomment to display our favorite wallpaper
#/usr/share/backgrounds/randwalls.sh & ## uncomment for a random system wall
feh --no-xinerama --bg-fill /usr/share/backgrounds/livarp.png

## launch conky --------------------------------------------------------
sleep 3 && conky -q -c ~/.conky/conkyrc_compiz &

## launch panel --------------------------------------------------------
tint2 &

## volume control ------------------------------------------------------
volumeicon &

## setup network -------------------------------------------------------
nm-applet &

## launch compiz -------------------------------------------------------
exec compiz ccp

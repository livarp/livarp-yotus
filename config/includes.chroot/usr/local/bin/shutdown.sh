#!/bin/bash

ACTION=`zenity --width=90 --height=200 --list --text="Sélectionner une action" --title="Logout" --column "Action" Eteindre Redemarrer Déconnecter`

if [ -n "${ACTION}" ]; then
  case $ACTION in
      Eteindre)
          dbus-send --system --print-reply --dest="org.freedesktop.ConsoleKit" /org/freedesktop/ConsoleKit/Manager org.freedesktop.ConsoleKit.Manager.Stop
          ;;
      Redemarrer)
          dbus-send --system --print-reply --dest="org.freedesktop.ConsoleKit" /org/freedesktop/ConsoleKit/Manager org.freedesktop.ConsoleKit.Manager.Restart
          ;;
      Déconnecter)
          xdotool key Ctrl+Alt+BackSpace
          ;;
  esac
fi
exit 0
